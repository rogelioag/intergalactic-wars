/*
 * GAME SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Game_Scene.hpp"

#include <cstdlib>
#include <basics/Canvas>
#include <basics/Director>

using namespace basics;
using namespace std;

namespace example
{
    // ---------------------------------------------------------------------------------------------
    // ID y ruta de las texturas que se deben cargar para esta escena. La textura con el mensaje de
    // carga está la primera para poder dibujarla cuanto antes:

    Game_Scene::Texture_Data Game_Scene::textures_data[] =
    {
        { ID(loading),    "game-scene/loading.png"        },
       /* { ID(hbar),       "game-scene/horizontal-bar.png" },
        { ID(vbar),       "game-scene/vertical-bar.png"   },
        { ID(player-bar), "game-scene/players-bar.png"    },
        { ID(ball),       "game-scene/ball.png"           },*/

        { ID(ship),       "game-scene/nave2.png"          },
        { ID(bullet),     "game-scene/disparo.png"        },
        { ID(enemy),      "game-scene/enemigo2.png"       },
        { ID(bkg),        "game-scene/space.png"          },
        { ID(bkg2),       "game-scene/space2.png"         },
        { ID(stars1),     "game-scene/stars.png"          },
        { ID(stars2),     "game-scene/stars2.png"         },
        { ID(bullet2),    "game-scene/disparo2.png"       },

        { ID(lives3),    "game-scene/lives3.png"          },
        { ID(lives2),    "game-scene/lives2.png"          },
        { ID(lives1),    "game-scene/lives1.png"          },

        { ID(asteroid),   "game-scene/asteroide.png"      },
    };

    // Pâra determinar el número de items en el array textures_data, se divide el tamaño en bytes
    // del array completo entre el tamaño en bytes de un item:

    unsigned Game_Scene::textures_count = sizeof(textures_data) / sizeof(Texture_Data);

    // ---------------------------------------------------------------------------------------------
    // Definiciones de los atributos estáticos de la clase:

    constexpr float Game_Scene::  ball_speed;
    constexpr float Game_Scene::player_speed;

    // ---------------------------------------------------------------------------------------------

    Game_Scene::Game_Scene()
    {
        // Se establece la resolución virtual (independiente de la resolución virtual del dispositivo).
        // En este caso no se hace ajuste de aspect ratio, por lo que puede haber distorsión cuando
        // el aspect ratio real de la pantalla del dispositivo es distinto.

        canvas_width  = 1280;
        canvas_height =  720;

        // Se inicia la semilla del generador de números aleatorios:

        srand (unsigned(time(nullptr)));

        // Se inicializan otros atributos:

        initialize ();
    }

    // ---------------------------------------------------------------------------------------------
    // Algunos atributos se inicializan en este método en lugar de hacerlo en el constructor porque
    // este método puede ser llamado más veces para restablecer el estado de la escena y el constructor
    // solo se invoca una vez.

    bool Game_Scene::initialize ()
    {
        state     = LOADING;
        suspended = true;
        gameplay  = UNINITIALIZED;

        canShoot = true;
        cadenciCount = 0;
        cadenci = 0.3;
        bulletVel = 1000;

        timeEnemySpawn = 0;
        enemySpeed = 300;
        minTime = 70;
        maxTime = 100;

        points = 0;
        lives = 3;

        timeBulletSpawn = 0;

        dificulty = 0;
        canUpdateDifficulty = true;

        timeAsteroidSpawn = 0;

        return true;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::suspend ()
    {
        suspended = true;               // Se marca que la escena ha pasado a primer plano
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::resume ()
    {
        suspended = false;              // Se marca que la escena ha pasado a segundo plano
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::handle (Event & event)
    {
        if (state == RUNNING)               // Se descartan los eventos cuando la escena está LOADING
        {
            if (gameplay == WAITING_TO_START)
            {
                start_playing ();           // Se empieza a jugar cuando el usuario toca la pantalla por primera vez
            }
            else switch (event.id)
            {
                case ID(touch-started):     // El usuario toca la pantalla
                case ID(touch-moved):
                {
                    user_target_x = *event[ID(x)].as< var::Float > ();
                    user_target_y = *event[ID(y)].as< var::Float > ();

                    follow_target = true;
                    cadenciCount += cadenci;
                    break;
                }

                case ID(touch-ended):       // El usuario deja de tocar la pantalla
                {
                    follow_target = false;
                    break;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::update (float time)
    {
        if (!suspended) switch (state)
        {
            case LOADING: load_textures  ();     break;
            case RUNNING: run_simulation (time); break;
            case ERROR:   break;
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::render (Context & context)
    {
        if (!suspended)
        {
            // El canvas se puede haber creado previamente, en cuyo caso solo hay que pedirlo:

            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            // Si no se ha creado previamente, hay que crearlo una vez:

            if (!canvas)
            {
                 canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            // Si el canvas se ha podido obtener o crear, se puede dibujar con él:

            if (canvas)
            {
                canvas->clear ();

                switch (state)
                {
                    case LOADING: render_loading   (*canvas); break;
                    case RUNNING: render_playfield (*canvas); break;
                    case ERROR:   break;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    // En este método solo se carga una textura por fotograma para poder pausar la carga si el
    // juego pasa a segundo plano inesperadamente. Otro aspecto interesante es que la carga no
    // comienza hasta que la escena se inicia para así tener la posibilidad de mostrar al usuario
    // que la carga está en curso en lugar de tener una pantalla en negro que no responde durante
    // un tiempo.

    void Game_Scene::load_textures ()
    {
        if (textures.size () < textures_count)          // Si quedan texturas por cargar...
        {
            // Las texturas se cargan y se suben al contexto gráfico, por lo que es necesario disponer
            // de uno:

            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                // Se carga la siguiente textura (textures.size() indica cuántas llevamos cargadas):

                Texture_Data   & texture_data = textures_data[textures.size ()];
                Texture_Handle & texture      = textures[texture_data.id] = Texture_2D::create (texture_data.id, context, texture_data.path);

                // Se comprueba si la textura se ha podido cargar correctamente:

                if (texture) context->add (texture); else state = ERROR;

                // Cuando se han terminado de cargar todas las texturas se pueden crear los sprites que
                // las usarán e iniciar el juego:
            }
        }
        else
        if (timer.get_elapsed_seconds () > 1.f)         // Si las texturas se han cargado muy rápido
        {                                               // se espera un segundo desde el inicio de
            create_sprites ();                          // la carga antes de pasar al juego para que
            restart_game   ();                          // el mensaje de carga no aparezca y desaparezca
                                                        // demasiado rápido.
            state = RUNNING;
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::create_sprites ()
    {
        // Se crean y configuran los sprites del fondo:

     /*   Sprite_Handle    top_bar(new Sprite( textures[ID(hbar)].get () ));
        Sprite_Handle middle_bar(new Sprite( textures[ID(vbar)].get () ));
        Sprite_Handle bottom_bar(new Sprite( textures[ID(hbar)].get () ));

           top_bar->set_anchor   (TOP | LEFT);
           top_bar->set_position ({ 0, canvas_height });
        middle_bar->set_anchor   (CENTER);
        middle_bar->set_position ({ canvas_width / 2.f, canvas_height / 2.f });
        bottom_bar->set_anchor   (BOTTOM | LEFT);
        bottom_bar->set_position ({ 0, 0 });

        sprites.push_back (   top_bar);
        sprites.push_back (middle_bar);
        sprites.push_back (bottom_bar);*/

        Sprite_Handle    bkg(new Sprite( textures[ID(bkg)].get () ));
        Sprite_Handle    bkg2(new Sprite( textures[ID(bkg2)].get () ));
        Sprite_Handle    strs1(new Sprite( textures[ID(stars1)].get () ));
        Sprite_Handle    strs2(new Sprite( textures[ID(stars2)].get () ));

        bkg->set_anchor    (BOTTOM | LEFT);
      //  bkg->set_position  ({0,0 });
        bkg2->set_anchor   (BOTTOM | LEFT);
     //   bkg2->set_position ({canvas_width-10, 0});

        strs1->set_anchor   (BOTTOM | LEFT);
     //   strs1->set_position ({0,0 });
        strs2->set_anchor   (BOTTOM | LEFT);
     //   strs2->set_position ({canvas_width-10, 0});

        sprites.push_back (   bkg);
        sprites.push_back (   bkg2);
        sprites.push_back (   strs1);
        sprites.push_back (   strs2);

        // Se crea el jugador y sus vidas:

      /*  Sprite_Handle  left_player_handle(new Sprite( textures[ID(player-bar)].get () ));
        Sprite_Handle right_player_handle(new Sprite( textures[ID(player-bar)].get () ));
        Sprite_Handle         ball_handle(new Sprite( textures[ID(ball)      ].get () ));

        sprites.push_back ( left_player_handle);
        sprites.push_back (right_player_handle);
        sprites.push_back (        ball_handle);*/

        Sprite_Handle  player_ship(new Sprite( textures[ID(ship)].get () ));
        sprites.push_back (player_ship);


    /*    top_border    =             top_bar.get ();
        bottom_border =          bottom_bar.get ();
        left_player   =  left_player_handle.get ();
        right_player  = right_player_handle.get ();
        ball          =         ball_handle.get ();*/

        Sprite_Handle  lives3(new Sprite( textures[ID(lives3)].get () ));
        Sprite_Handle  lives2(new Sprite( textures[ID(lives2)].get () ));
        Sprite_Handle  lives1(new Sprite( textures[ID(lives1)].get () ));

        lives3->set_anchor    (TOP | LEFT);
        lives3->set_position  ({100,canvas_height - 40 });
        lives2->set_anchor    (TOP | LEFT);
        lives2->set_position  ({100,canvas_height - 40 });
        lives1->set_anchor    (TOP | LEFT);
        lives1->set_position  ({100,canvas_height - 40 });

        sprites.push_back (lives3);
        sprites.push_back (lives2);
        sprites.push_back (lives1);


        // Se guardan punteros a los sprites que se van a usar frecuentemente:
        background1 = bkg.get();
        background2 = bkg2.get();
        stars1 = strs1.get();
        stars2 = strs2.get();

        playerShip = player_ship.get();

        playerLives3 = lives3.get();
        playerLives2 = lives2.get();
        playerLives1 = lives1.get();
    }

    // ---------------------------------------------------------------------------------------------
    // Cuando el juego se inicia por primera vez o cuando se reinicia porque un jugador pierde, se
    // llama a este método para restablecer la posición y velocidad de los sprites:

    void Game_Scene::restart_game()
    {
     /*    left_player->set_position ({ left_player->get_width () * 3.f, canvas_height / 2.f });
         left_player->set_speed_y  (0.f);
        right_player->set_position ({ canvas_width  - right_player->get_width () * 3.f, canvas_height / 2.f });
        right_player->set_speed_y  (0.f);
                ball->set_position ({ canvas_width / 2.f, canvas_height / 2.f });
                ball->set_speed    ({ 0.f, 0.f });*/

        timer.reset ();

        enemies.clear();
        bullets.clear();
        enemyBullets.clear();
        asteroids.clear();


        playerShip->set_position({150,canvas_height / 2.f});
        background1->set_position  ({0,0 });
        background2->set_position ({canvas_width-10, 0});
        stars1->set_position ({0,0 });
        stars2->set_position ({canvas_width-10, 0});

        canShoot = true;
        cadenciCount = 0;
        cadenci = 0.3;
        bulletVel = 1000;

        timeEnemySpawn = 0;
        enemySpeed = 300;
        minTime = 70;
        maxTime = 100;

        points = 0;
        lives = 3;

        timeBulletSpawn = 0;

        dificulty = 0;
        canUpdateDifficulty = true;

        timeAsteroidSpawn = 0;

        follow_target = false;

        gameplay = WAITING_TO_START;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::start_playing ()
    {
        // Se genera un vector de dirección al azar:

     /*   Vector2f random_direction
        (
            float(rand () % int(canvas_width ) - int(canvas_width  / 2)),
            float(rand () % int(canvas_height) - int(canvas_height / 2))
        );

        // Se hace unitario el vector y se multiplica un el valor de velocidad para que el vector
        // resultante tenga exactamente esa longitud:

     //   ball->set_speed (random_direction.normalized () * ball_speed);*/

        gameplay = PLAYING;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::run_simulation (float time)
    {
        // Se actualiza el estado de todos los sprites:

        for (auto & sprite : sprites)
        {
            sprite->update (time);
        }
        for (auto & sprite : bullets)
        {
            sprite->update (time);
        }
        for (auto & sprite : enemies)
        {
            sprite->update (time);
        }
        for (auto & sprite : enemyBullets)
        {
            sprite->update (time);
        }
        for (auto & sprite : asteroids)
        {
            sprite->update (time);
        }


        update_user ();
        updateBullet();
        moveBackground();

        createEnemies();
        moveEnemies();
        checkCollicionEnemiesBullets();
        progresionDeDificultad();

        createAsteroid();
        checkCollisionAsteroids();
      //  update_ai   ();


        // Se comprueban las posibles colisiones de la bola con los bordes y con los players:

      //  check_ball_collisions ();

    }

    // ---------------------------------------------------------------------------------------------
    // Usando un algoritmo sencillo se controla automáticamente el comportamiento del jugador
    // izquierdo.

  /*  void Game_Scene::update_ai ()
    {
        if (left_player->intersects (*top_border))
        {
            // El player izquierdo ha tocado el borde superior, por lo que se debe quedar quieto:

            left_player->set_position_y (top_border->get_bottom_y () - left_player->get_height () / 2.f);
            left_player->set_speed_y (0.f);
        }
        else
        if (left_player->intersects (*bottom_border))
        {
            // El player izquierdo ha tocado el borde inferior, por lo que se debe quedar quieto:

            left_player->set_position_y (bottom_border->get_top_y () + left_player->get_height () / 2.f);
            left_player->set_speed_y (0.f);
        }
        else
        {
            // Se determina si la bola está por encima o por debajo del centro del player izquierdo
            // para establecer si debe subir o bajar:

            float delta_y = ball->get_position_y () - left_player->get_position_y ();

            if (ball->get_speed_y () < 0.f)
            {
                if (delta_y < 0.f)
                {
                    left_player->set_speed_y (-player_speed * (ball->get_speed_x () < 0.f ? 1.f : .5f));
                }
                else
                    left_player->set_speed_y (0.f);
            }
            else
            if (ball->get_speed_y () > 0.f)
            {
                if (delta_y > 0.f)
                {
                    left_player->set_speed_y (+player_speed * (ball->get_speed_x () < 0.f ? 1.f : .5f));
                }
                else
                    left_player->set_speed_y (0.f);
            }
        }
    }*/

    // ---------------------------------------------------------------------------------------------
    // Se hace que el player dechero se mueva hacia la direccion según el usuario esté
    // tocando la pantalla ya sea diferente del centro. Cuando el usuario no toca la
    // pantalla se deja al player quieto.

    void Game_Scene::update_user ()
    {
       /* if (playerShip->intersects (*top_border))
        {
            // Si el player está tocando el borde superior, no puede ascender:

            playerShip->set_position_y (top_border->get_bottom_y () - playerShip->get_height () / 2.f);
            playerShip->set_speed_y (0);
        }
        else
        if (playerShip->intersects (*bottom_border))
        {
            // Si el player está tocando el borde inferior, no puede descender:

            playerShip->set_position_y (bottom_border->get_top_y () + playerShip->get_height () / 2.f);
            playerShip->set_speed_y (0);
        }
        else*/
        switch (lives)
        {
            case 0:
                playerLives1->hide();
                playerLives2->hide();
                playerLives3->hide();
                break;
            case 1:
                playerLives1->show();
                playerLives2->hide();
                playerLives3->hide();
                break;
            case 2:
                playerLives1->hide();
                playerLives2->show();
                playerLives3->hide();
                break;
            case 3:
                playerLives1->hide();
                playerLives2->hide();
                playerLives3->show();
                break;
        }


        if (follow_target)
        {
            if(canShoot)
            {
                /// Se guardan los sprites de las balas y se pintan al crearse
                Sprite_Handle bullet(new Sprite(textures[ID(bullet)].get()));
                bullet->set_anchor(BOTTOM | LEFT);
                bullet->set_position({playerShip->get_position_x(), playerShip->get_position_y()});
                bullets.push_back(bullet);
                canShoot = false;
            }

            /// Se limita la creacion de balas a un tiempo de cadencia
            if (cadenciCount >= 6)
            {
                canShoot = true;
                cadenciCount = 0;
            }


            // Si el usuario está tocando la pantalla, se determina en que direccion para que el
            // jugador vaya a esa direccion.

            float delta_y = user_target_y - playerShip->get_position_y ();
            float delta_x = user_target_x - playerShip->get_position_x();

            if (delta_y < 0.f) playerShip->set_speed_y (-player_speed); else
            if (delta_y > 0.f) playerShip->set_speed_y (+player_speed);

            if (delta_x < 0.f) playerShip->set_speed_x (-player_speed); else
            if (delta_x > 0.f) playerShip->set_speed_x (+player_speed);
        }
        else {
            playerShip->set_speed_y(0);
            playerShip->set_speed_x(0);
        }


        if(lives < 1)
        {
            state = LOADING;
            restart_game();
        }
    }

    // ---------------------------------------------------------------------------------------------

  /*  void Game_Scene::check_ball_collisions ()
    {
        // Se comprueba si la bola choca con el borde superior o con el inferior, en cuyo caso se
        // ajusta su posición para que no lo atraviese y se invierte su velocidad en el eje Y:

        if (ball->intersects (*top_border))
        {
            ball->set_position_y (top_border->get_bottom_y () - ball->get_height() / 2.f);
            ball->set_speed_y    (-ball->get_speed_y ());
        }

        if (ball->intersects (*bottom_border))
        {
            ball->set_position_y (bottom_border->get_top_y () + ball->get_height() / 2.f);
            ball->set_speed_y    (-ball->get_speed_y ());
        }

        // Solo si la bola no ha superado alguno de los players, se comprueba si choca con alguno
        // de ellos, en cuyo caso se ajusta su posición para que no los atraviese y se invierte su
        // velocidad en el eje X:

        if (gameplay != BALL_LEAVING)
        {
            if (ball->get_left_x () < left_player->get_right_x ())
            {
                if (ball->get_bottom_y () < left_player->get_top_y () && ball->get_top_y () > left_player->get_bottom_y ())
                {
                    ball->set_position_x (left_player->get_right_x () + ball->get_width() / 2.f);
                    ball->set_speed_x    (-ball->get_speed_x ());
                }
                else
                    gameplay = BALL_LEAVING;
            }

            if (ball->get_right_x () > right_player->get_left_x ())
            {
                if (ball->get_bottom_y () < right_player->get_top_y () && ball->get_top_y () > right_player->get_bottom_y ())
                {
                    ball->set_position_x (right_player->get_left_x () - ball->get_width() / 2.f);
                    ball->set_speed_x    (-ball->get_speed_x ());
                }
                else
                    gameplay = BALL_LEAVING;
            }
        }
        else
        if (ball->get_right_x () < 0.f || ball->get_left_x () > float(canvas_width))
        {
            restart_game ();
        }
    }*/

    // ---------------------------------------------------------------------------------------------
    /// Mueve la bala a velocidad constante y detecta sus colisiones
  void Game_Scene::updateBullet()
  {
      for (auto & sprite : bullets)
      {
          sprite->set_speed_x (bulletVel);

          if(sprite->get_left_x() > canvas_width)
          {
              ///Se valida si la bala colisiona con el limite de la pantalla

             // sprite->set_speed_x (0);
              bullets.remove(sprite);
             // bullets.pop_front();
             // sprite->hide();

          }

          for (auto & sprite2 : enemies)
          {
              ///Se valida si la bala colisiona con un enemigo
              ///Se elimina y se saca de la lista la bala y el enemigo con el que colisiona
              ///Se suman puntos.
              if(sprite->intersects(*sprite2) )
              {
                 // sprite->set_speed_x (0);
                 // sprite->hide();
                 // sprite2->hide();

                  points +=1;
                  bullets.remove(sprite);
                  enemies.remove(sprite2);
              }
          }
      }

  }
    // ---------------------------------------------------------------------------------------------
    ///Se mueve el fondo
    void Game_Scene::moveBackground()
    {
        ///Se asigna la velocidad a la primera capa del fondo.
        background1->set_speed_x(-30);
        background2->set_speed_x(-30);

        if(background1->get_right_x()< 0)
        {
            background1->set_position_x(canvas_width-10);
        }
        if(background2->get_right_x()< 0)
        {
            background2->set_position_x(canvas_width-10);
        }

        ///Se asigna la velocidad a la seguna capa del fondo para el efecto parallax.
        stars1->set_speed_x(-50);
        stars2->set_speed_x(-50);

        if(stars1->get_right_x()< 0)
        {
            stars1->set_position_x(canvas_width-10);
        }
        if(stars2->get_right_x()< 0)
        {
            stars2->set_position_x(canvas_width-10);
        }
    }

    // ---------------------------------------------------------------------------------------------
    ///Se crean los enemigos cada cierto tiempo
    void Game_Scene::createEnemies()
    {
        timeEnemySpawn += 0.5;

       /* if(int(timer.get_elapsed_seconds ()) % 10 == 0)
        {
            timepoLimite =  int(rand()% (maxTime ) + (minTime = 10));
        } else
        {
           // timepoLimite =  int((rand()%100 + 70));
        }*/

        timepoLimite =  int(rand()% (maxTime ) + (minTime));

        if(timeEnemySpawn > timepoLimite)
        {
            float randomPos;

            Sprite_Handle enem(new Sprite(textures[ID(enemy)].get()));
            enem->set_anchor(BOTTOM | LEFT);
            randomPos = rand()% int((canvas_height - enem->get_top_y ())) + int(0+enem->get_bottom_y ());

            enem->set_position({canvas_width - 30, randomPos});
            enemies.push_back(enem);

            enemy = enem.get();

            timeEnemySpawn = 0;
        }
    }

    // ---------------------------------------------------------------------------------------------
    ///Hace que se muevan los enemigos
    void Game_Scene::moveEnemies()
    {


        for (auto & sprite : enemies)
        {
            timeBulletSpawn += 0.5;

            if(timeBulletSpawn > 6)
            {
                createEnemiesBullets(*sprite);
                timeBulletSpawn = 0;
            }

            sprite->set_speed_x (-enemySpeed);

            if(sprite->get_left_x() < 0)
            {
                enemies.remove(sprite);
              //  sprite->set_speed_x (0);
             //   sprite->hide();
            }

            if(sprite->intersects(*playerShip))
            {
                enemies.remove(sprite);
                lives -=1;
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    /// Se crean las balas de los enemigos con direccion aleatoria
    void Game_Scene::createEnemiesBullets(const Sprite & enemy)
    {
        Vector2f random_direction
                (
                        float(rand () % int(canvas_width ) - int(canvas_width  / 2)),
                        float(rand () % int(canvas_height) - int(canvas_height / 2))
                );

        Sprite_Handle bulletEnemy(new Sprite(textures[ID(bullet2)].get()));
        bulletEnemy->set_anchor(CENTER);

        bulletEnemy->set_position({enemy.get_position_x(), enemy.get_position_y() + (enemy.get_height()/2)});
        enemyBullets.push_back(bulletEnemy);

        enemy_bullet = bulletEnemy.get();
       // enemy_bullet->set_speed_x(-700);
        enemy_bullet->set_speed(random_direction.normalized () * ball_speed);
    }

    // ---------------------------------------------------------------------------------------------
    /// Aqui se valida si las balas de los enemigos salen de la pantalla o chocan con el jugador para bajarle vida
    void Game_Scene::checkCollicionEnemiesBullets()
    {
        for (auto & sprite : enemyBullets)
        {
            if(sprite->get_bottom_y() > canvas_height || sprite->get_left_x() > canvas_width || sprite->get_right_x() < 0 || sprite->get_top_y() < 0)
            {
                enemyBullets.remove(sprite);
            }
            else if(sprite->intersects(*playerShip))
            {
                enemyBullets.remove(sprite);
                lives -= 1;
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    /// Se crean los asteroides con movimiento y posicion aleatoria
    void Game_Scene::createAsteroid()
    {
        timeAsteroidSpawn += 0.5;

        if(timeAsteroidSpawn >= 100)
        {
            Vector2f random_direction
                    (
                            float(rand() % int(canvas_width) - int(canvas_width / 2)),
                            float(rand() % int(canvas_height) - 0)
                    );

            Sprite_Handle asteroid(new Sprite(textures[ID(asteroid)].get()));
            asteroid->set_anchor(CENTER);

            asteroid->set_position({rand() % int(canvas_width - 100) -(100), 0});
            asteroids.push_back(asteroid);

            asteroide = asteroid.get();
            // enemy_bullet->set_speed_x(-700);
            asteroide->set_speed(random_direction.normalized() * 250);

            timeAsteroidSpawn = 0;
        }
    }


    void Game_Scene::checkCollisionAsteroids()
    {
        for (auto & sprite : asteroids)
        {
            if (sprite->get_bottom_y() > canvas_height || sprite->get_left_x() > canvas_width || sprite->get_right_x() < 0)
            {
                asteroids.remove(sprite);
            }
            else if (sprite->intersects(*playerShip))
            {
                asteroids.remove(sprite);
                lives -= 1;
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    /// Aqui se aumenta la dificultad conforme pasa el juego.
    /// Se hace que aparezcan los enemigos con menor tiempo de espera.
    void Game_Scene::progresionDeDificultad()
    {
       if(timer.get_elapsed_seconds () > 10)
        {
            dificulty = 1;
            if(timer.get_elapsed_seconds () > 30)
            {
                dificulty = 2;
                if(timer.get_elapsed_seconds () > 60)
                {
                    dificulty = 3;
                    if(timer.get_elapsed_seconds () > 90)
                    {
                        dificulty = 4;
                        if(timer.get_elapsed_seconds () > 120)
                        {
                            dificulty = 5;
                            if(timer.get_elapsed_seconds () > 150)
                            {
                                dificulty = 6;
                                if(timer.get_elapsed_seconds () > 180)
                                {
                                    dificulty = 7;
                                    if(timer.get_elapsed_seconds () > 210)
                                    {
                                        dificulty = 8;
                                        if(timer.get_elapsed_seconds () > 240)
                                        {
                                            dificulty = 9;
                                            if(timer.get_elapsed_seconds () > 270)
                                            {
                                                dificulty = 10;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



        switch (dificulty)
        {
            case 0:
                maxTime = 100;
                minTime = 70;
                break;
            case 1:
                maxTime = 90;
                minTime = 60;
                break;
            case 2:
                maxTime = 80;
                minTime = 50;
                break;
            case 3:
                maxTime = 70;
                minTime = 40;
                break;
            case 4:
                maxTime = 60;
                minTime = 30;
                break;
            case 5:
                maxTime = 50;
                minTime = 20;
                break;
            case 6:
                maxTime = 40;
                minTime = 10;
                break;
            case 7:
                maxTime = 30;
                minTime = 1;
                break;
            case 8:
                maxTime = 10;
                minTime = 1;
                break;
            case 9:
                maxTime = 3;
                minTime = 1;
                break;
            case 10:
                maxTime = 1;
                minTime = 0;
                break;

        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::render_loading (Canvas & canvas)
    {
        Texture_2D * loading_texture = textures[ID(loading)].get ();

        if (loading_texture)
        {
            canvas.fill_rectangle
            (
                { canvas_width * .5f, canvas_height * .5f },
                { loading_texture->get_width (), loading_texture->get_height () },
                  loading_texture
            );
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Simplemente se dibujan todos los sprites que conforman la escena.

    void Game_Scene::render_playfield (Canvas & canvas)
    {
        for (auto & sprite : sprites)
        {
            sprite->render (canvas);
        }
        for (auto & sprite : bullets)
        {
            sprite->render (canvas);
        }
        for (auto & sprite : enemies)
        {
            sprite->render (canvas);
        }
        for (auto & sprite : enemyBullets)
        {
            sprite->render (canvas);
        }
        for (auto & sprite : asteroids)
        {
            sprite->render (canvas);
        }
    }

}
